﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModel
{
    public class BMIData
    {
            [Display(nameof = "體重")]
            [Required(ErrorMessage = "必填欄位")]
            [Range(30, 150, ErrorMessage = "請輸入30~150的數值")]
            public float Weight { get; set; }

            [Display(nameof = "身高")]
            [Required(ErrprMessage = "必填欄位")]
        [Range(50, 250, ErrorMessage = "請輸入30~150的數值")]
        public float Height { get; set; }
            public float BMI { get; set; }
            public string Level { get; set; }
     
    }
}