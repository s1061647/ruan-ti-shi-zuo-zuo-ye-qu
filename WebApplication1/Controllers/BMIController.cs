﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModel;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data )
        {
            float m_hh =  data.Height/ 100;
            float bmi = data.Weight/ (m_hh * m_hh);

            string level = "";
            if (bmi < 18.5)
            {
                level = "體重過輕";
            }
            else if (18.5 <= bmi && bmi < 24)
            {
                level = "正常範圍";
            }
            else if (24 <= bmi && bmi < 27)
            {
                level = "過重";
            }
            else if (27 <= bmi && bmi < 30)
            {
                level = "輕度肥胖";
            }
            else if (30 <= bmi && bmi < 35)
            {
                level = "中度肥胖";
            }
            else if (35 <= bmi)
            {
                level = "重度肥胖";
            }

            data.BMI = bmi;
            data.Level = level;
            return View(data);
        }
    }
}